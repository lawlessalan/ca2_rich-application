﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlanLawless_CA2.Models;
using System.Net;

namespace AlanLawless_CA2.Controllers
{
 
    public class HomeController : Controller
    {

        MoviesDB db = new MoviesDB();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            
            return View(db.Movies.ToList());
        }

        public ActionResult ListOfActors(string orderOfActors)
        {
            if (orderOfActors == null) orderOfActors = "a_to_z";
            ViewBag.noOfMovies = (orderOfActors == "most") ? "least" : "most";
            ViewBag.DOB = (orderOfActors == "oldest") ? "youngest" : "oldest";
            ViewBag.Name = (orderOfActors == "a_to_z") ? "z_to_a" : "a_to_z";
            
            IQueryable<Actor> actors = db.Actors;

            switch (orderOfActors)
            {
                case "a_to_z":
                    ViewBag.Name = "z_to_a";
                    //actors = actors.OrderByDescending(c => c.Name).Include("ActorMovies");
                    actors = actors.OrderByDescending(c => c.Name);
                    break;

                case "z_to_a":
                    ViewBag.Name = "a_to_z";
                    //actors = actors.OrderByDescending(c => c.Name).Include("ActorMovies");
                    actors = actors.OrderBy(c => c.Name);
                    break;
                case "oldest":
                    ViewBag.DOB = "youngest";
                    actors = actors.OrderByDescending(c => c.DOB.Year);
                    break;
                case "youngest":
                    ViewBag.DOB = "oldest";
                    actors = actors.OrderBy(c => c.DOB);
                    break;
                /*case "most":
                    ViewBag.DOB = "least";
                    actors = actors.OrderByDescending(c => c.ActorMovies.Count).Include("ActorMovies");
                    break;
                case "least":
                    ViewBag.DOB = "most";
                    actors = actors.OrderBy(c => c.ActorMovies.Count).Include("ActorMovies");
                    break;*/
                default:
                    ViewBag.Name = "a_to_z";
                    //actors = actors.OrderByDescending(c => c.Name).Include("ActorMovies");
                    actors = actors.OrderBy(c => c.Name);
                    break;
            }
            
            
            return View(actors.ToList());
        }    

        public PartialViewResult MoviesActorsById(int id)
        {

            var actor = db.Actors.Include("ActorMovies").SingleOrDefault(a =>a.ActorId==id);

            return PartialView("_ListMovies1", actor);

        }

        //DisplayEditMovie DisplayEditMovieSubmit

        [HttpGet]
        public ActionResult DisplayEditMovieSubmit()
        {
            return RedirectToAction("Index");

        }


        [HttpPost]
        public ActionResult DisplayEditMovieSubmit(Movie editMovie)
        {
            try
            {

                    db.Entry(editMovie).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                
            }
            catch
            {
                return View();
            }
        }

        //DisplayDeleteMovieSubmit

        [HttpGet]
        public ActionResult DisplayDeleteMovieSubmit()
        {
            return RedirectToAction("Index");

        }


        [HttpPost]
        public ActionResult DisplayDeleteMovieSubmit(Movie deleteMovie)
        {
            
            try
            {
                if (deleteMovie.MovieId != null)
                {
               
                db.Movies.Remove(db.Movies.Find(deleteMovie.MovieId));
                //db.SaveChanges();
                
                    /*when Delete Movie button is deleted the Moive is deleted from the database.
                     * however, I could not get the page to reset. So if I deleted a Movie and you pushed
                     * a button (ie the delete button again) then a run time error would occur*/

                return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }


        public PartialViewResult DisplayDeleteMovie(int id)
        {
            var movie = db.Movies.Find(id);
            return PartialView("DisplayDeleteMovie", movie);
        }

        public PartialViewResult DisplayEditMovie(int id)
        {
            var movie = db.Movies.Find(id);
            return PartialView("DisplayEditMovie", movie);
        }


        public PartialViewResult DisplayMovieDescription(int id)
        {
            var movie = db.Movies.Find(id);
            return PartialView("_DisplayMovie", movie);
        }

        public PartialViewResult ActorsById(int id)
        {
            var actor = db.Actors.Find(id);
            return PartialView("_GetActorInfo",actor);

        }
        
        public ActionResult Details(int id)
        {
            return View(db.Movies.Include("ActorsInMovie").SingleOrDefault(mov => mov.MovieId == id));
        }

        //
        // GET: /Home/Create

        
       
        [HttpGet]
        public ActionResult AddingNewMovieSubmit()
        {
            return RedirectToAction("Index");

        }


        [HttpPost]
        public ActionResult AddingNewMovieSubmit(Movie addNewMovie)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    db.Movies.Add(addNewMovie);
                    db.SaveChanges();

                    //return RedirectToAction("Index");
                    return View(db.Movies.ToList());
                    
                }
                else return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public PartialViewResult AddingNewMovie()
        {       
            return PartialView("AddingNewMovie");
            
        }

        public PartialViewResult AddingNewActor()
        {
            return PartialView("AddingNewActor");

        }

        [HttpGet]
        public ActionResult AddingNewActorSubmit()
        {
            return RedirectToAction("Index");

        }//AddingActorToMovieSubmit



        [HttpGet]
        public ActionResult AddingActorToMovieSubmit()
        {
            return RedirectToAction("Index");

        }//AddingActorToMovieSubmit

        [HttpPost]
        public ActionResult AddingActorToMovieSubmit(MoviesActor addNewMoviesActor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.MovieActors.Add(addNewMoviesActor);
                    db.SaveChanges();

                    //return RedirectToAction("Index");
                    return View(db.Movies.ToList());

                }
                else return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }



        [HttpPost]
        public ActionResult AddingNewActorSubmit(Actor addNewActor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Actors.Add(addNewActor);
                    db.SaveChanges();

                    //return RedirectToAction("Index");
                    return View(db.Movies.ToList());

                }
                else return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public PartialViewResult AddingActorToMovie()   //correct adding new movie
        {
            ViewBag.Actors = db.Actors.ToList();
            ViewBag.Movies = db.Movies.ToList();
            return PartialView("AddingActorToMovie");

        }


        [HttpGet]
        public ActionResult CreateMovieActor()
        {
            ViewBag.Actors = db.Actors.ToList();
            ViewBag.Movies = db.Movies.ToList();
            return View();

        }
        [HttpPost]
        public ActionResult CreateMovieActor(MoviesActor movieActor)
        { try
            {
                if (ModelState.IsValid)
                {
                    db.MovieActors.Add(movieActor);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                else return View();
            }
            catch
            {
                return View();
            }
        }


        

        
        //
        // POST: /Home/Create
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Actors = db.Actors.ToList();
                return View();
            
        }
       
        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Delete/5
        
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Home/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
