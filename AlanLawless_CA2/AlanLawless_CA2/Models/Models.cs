﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlanLawless_CA2.Models
{

        public enum Genre { Action, Thriller, SciFi, Romance }
        public enum Gender { Female, Male }
        
        

        public class MovieDBInitialiser : DropCreateDatabaseAlways<MoviesDB>
        {
            public Actor ChBale =
                    new Actor()
                    {
                        Name = "Christian Bale",
                        DOB = DateTime.Parse("30/01/1974"),
                        Sex=Gender.Male,
                        Image = "http://www.details.com/images/celebrities-entertainment/cover-stars/200805/CELEBRITIES_christian_bale_300co.jpg"
                    };

            public Actor MatMc = new Actor()
            {
                Name = "Matthew McConaughey",
                DOB = DateTime.Parse("4/11/1969"),
                Sex = Gender.Male,
                Image = "http://a5.files.biography.com/image/upload/c_fill,dpr_1.0,g_face,h_300,q_80,w_300/MTE5NTU2MzE2NDk3NjEwMjUx.jpg"
            };

            public Actor AnnHath = new Actor()
            {
                Name = "Anne Hathaway",
                Image = "http://img2.wikia.nocookie.net/__cb20130929173200/lesmiserables/images/9/91/Anne-Hathaway-anne-hathaway-548747_1137_1600.jpg",
                Sex = Gender.Female,
                DOB = DateTime.Parse("12/11/1982")
            };

            public Actor LeoDe = new Actor()
            {
                Name = "Leonardo DiCaprio",
                Sex = Gender.Male,
                DOB = DateTime.Parse("11/11/1974"),
                Image = "http://www.hollywoodreporter.com/sites/default/files/imagecache/news_portrait/2014/10/Leonardo_DiCaprio.jpg"
            };

            public Actor SamJac = new Actor()
            {
                Name = "Samuel L. Jackson",
                Sex = Gender.Male,
                DOB = DateTime.Parse("21/12/1948"),
                Image = "http://www.standard.co.uk/news/standard-pictures/article7341699.ece/alternates/w620/Samuel%20L%20Jackson.jpg"
            };

            //

            public Actor UmaThur = new Actor()
            {
                Name = "Uma Thurman",
                Sex = Gender.Female,
                DOB = DateTime.Parse("29/04/1970"),
                Image = "http://upload.wikimedia.org/wikipedia/commons/b/be/Uma_Thurman_2014_(cropped).jpg"
            };

            public Actor MarCot = new Actor()
            {
                Name = "Marion Cotillard",
                Sex = Gender.Female,
                DOB = DateTime.Parse("30/09/1975"),
                Image = "http://ia.media-imdb.com/images/M/MV5BMTQxNTEzNTkwNF5BMl5BanBnXkFtZTcwNzQ2NDIwOQ@@._V1_SX214_CR0,0,214,317_AL_.jpg"
            };
            //

            public Actor LuLi = new Actor()
            {
                Name = "Lucy Liu",
                Sex = Gender.Female,
                DOB = DateTime.Parse("02/12/1968"),
                Image = "http://img3.wikia.nocookie.net/__cb20140519034303/fanon/images/c/c6/Lucy-liu-93833.jpg"
            };

            protected override void Seed(MoviesDB context)
            {
                var actorList = new List<Actor>(){ChBale, MatMc,AnnHath};
                
                actorList.ForEach(act => context.Actors.Add(act));
                context.SaveChanges();

                var Films = new List<Movie>()
                {
                    new Movie()
                    {
                        Title="Intersteller", Director = "Christopher Nolan",
                        MovieGenre=Genre.SciFi, Year=2014, StarRating=5,
                        Description="A team of explorers travel through a wormhole in an attempt to find a potentially habitable planet that will sustain humanity.",
                        MovieImage="http://ia.media-imdb.com/images/M/MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1_SX640_SY720_.jpg",
                        MoviePreview="//www.youtube.com/embed/0vxOhd4qlnA",
                        ActorsInMovie = new List<MoviesActor>{
                            new MoviesActor(){ Actor = MatMc, ScreenName="Cooper"},
                            new MoviesActor(){ Actor = AnnHath, ScreenName="Brand"}
                        }
                    },
                    new Movie()
                    {
                        Title="The Dark Knight Rises", Director = "Christopher Nolan",
                        MovieGenre=Genre.Action, Year=2012, StarRating=4, MoviePreview="//www.youtube.com/embed/g8evyE9TuYk",
                        Description="When Bane, a former member of the League of Shadows, plans to continue the work of Ra's al Ghul, the Dark Knight is forced to return after an eight year absence to stop him.",
                        MovieImage="http://img4.wikia.nocookie.net/__cb20111211002330/batman/images/9/91/TheDarkKnightRises_Poster.jpg",
                        ActorsInMovie=new List<MoviesActor>{
                            new MoviesActor(){ Actor = AnnHath, ScreenName="Catwoman"},
                            new MoviesActor(){ Actor=ChBale, ScreenName="Batman"},
                            new MoviesActor(){ Actor=MarCot,ScreenName="Miranda"}
                        }
                    },

                    new Movie()
                    {
                        Title="Kill Bill", Director="Quinton Tarantino",
                        MovieGenre=Genre.Thriller, MovieImage="http://la-screenwriter.com/wp-content/uploads/2012/03/kill-bill-vol-1-poster-01.jpg",
                        Description="The Bride wakens from a four-year coma. The child she carried in her womb is gone. Now she must wreak vengeance on the team of assassins who betrayed her - a team she was once part of.",
                        Year=2003, StarRating=3,
                        MoviePreview="//www.youtube.com/embed/ot6C1ZKyiME", ActorsInMovie = new List<MoviesActor>{
                            new MoviesActor(){Actor=UmaThur, ScreenName="The Bride"},
                            new MoviesActor(){Actor = LuLi, ScreenName="O-Ren Ishii"}
                        }
                    },
                    new Movie()
                    {
                        Title="Django Unchained", Director="Quinton Tarantino",
                        MovieGenre=Genre.Action, MovieImage="http://www.thewrap.com/sites/default/files/DjangoUnchainedInside2_0.png",
                        Description="With the help of a German bounty hunter, a freed slave sets out to rescue his wife from a brutal Mississippi plantation owner.",
                        MoviePreview="//www.youtube.com/embed/eUdM9vrCbow",
                        Year=2012, StarRating=5,
                        ActorsInMovie = new List<MoviesActor>
                        {
                            new MoviesActor(){Actor=SamJac, ScreenName="Stephen"},
                            new MoviesActor(){Actor=LeoDe, ScreenName="Calvin Candie"}
                        }
                    },

                    new Movie()
                    {
                        Title="Pulp Fiction", Description="The lives of two mob hit men, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.",
                        MovieGenre=Genre.Action, MovieImage="http://upload.wikimedia.org/wikipedia/en/8/82/Pulp_Fiction_cover.jpg", Year=1994, Director="Quinton Tarantino",
                        StarRating=4, MoviePreview="//www.youtube.com/embed/dZWPL9deY7I", ActorsInMovie = new List<MoviesActor>
                        {
                            new MoviesActor(){Actor=SamJac, ScreenName="Jules"},
                            new MoviesActor(){Actor=UmaThur, ScreenName = "Mia Wallace"}
                        }
                    },

                    new Movie()
                    {
                        Title = "Inception", Description="A thief who steals corporate secrets through use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO",
                        MovieGenre=Genre.Romance, MovieImage="http://ia.media-imdb.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX640_SY720_.jpg,", Director="Christopher Nolan",
                        StarRating=4, Year=2010, MoviePreview="//www.youtube.com/embed/66TuSJo4dZM", ActorsInMovie = new List<MoviesActor>
                        {
                            new MoviesActor(){Actor=LeoDe, ScreenName="Cobb"},
                            new MoviesActor(){Actor=MarCot, ScreenName="Mal"}
                        }
                    }
                };

                Films.ForEach(mov=>context.Movies.Add(mov));
                context.SaveChanges();
            }
        }
        public class Movie
        {
            [Key]
            public int MovieId { get; set; }
            [Required]
            public string Title { get; set; }
            public string Description { get; set; }
            public string Director { get; set; }
            public Genre MovieGenre { get; set; }
            public int Year { get; set; }
            [AllowHtml]
            public string MovieImage { get; set; }
            [AllowHtml]
            public string MoviePreview { get; set; }
            public int StarRating { get; set; }

            public virtual List<MoviesActor> ActorsInMovie{get;set;}
            public virtual List<Actor> theActors{
                get {return (ActorsInMovie==null)?null:ActorsInMovie.Select(am=>am.Actor).ToList();}
            }
        }

        public class Actor
        {
            [Key]
            public int ActorId { get; set; }
            public string Name { get; set; }
            [DisplayName("Born"), DataType(DataType.Date),
                    DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
            public DateTime DOB { get; set; }
            public Gender Sex { get; set; }
            public string Image { get; set; }
            public List<MoviesActor> ActorMovies { get; set; }
            public virtual List<Movie> theMovies
            {
                get { return (ActorMovies == null) ? null : ActorMovies.Select(am => am.Movie).ToList(); }
            }

        }

        public class MoviesActor
        {
            [Key, Column(Order = 0)]
            public int MovieId { get; set; }
            [Key, Column(Order = 1)]
            public int ActorId { get; set; }
            public string ScreenName { get; set; }

            public virtual Movie Movie { get; set; }
            public virtual Actor Actor { get; set; }

        }

        public class MoviesDB : DbContext
        {
            public DbSet<Actor> Actors { get; set; }
            public DbSet<Movie> Movies { get; set; }
            public DbSet<MoviesActor> MovieActors { get; set; }
            public MoviesDB() : base("MoviesDB") { }
            //public CampIntities() : base("CampDb") { }
        }
        
    }
